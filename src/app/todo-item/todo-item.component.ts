import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
    selector: 'app-todo-item',
    templateUrl: 'todo-item.component.html'
})

export class TodoItemComponent implements OnInit {
    @Input() todo;
    todoForm: FormGroup;

    ngOnInit() { }
}

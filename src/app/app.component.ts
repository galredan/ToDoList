import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';
  public todos = [{
    id: 0,
    title: 'Murène',
    content: 'Sucrée',
    isActive: false
  }];
  public todoForm: FormGroup;
  constructor() {
    this.todoForm = new FormGroup({
      title: new FormControl(),
      content: new FormControl(),
      isActive: new FormControl()
    });
  }

  onSubmit() {
    const todo = {
      id: this.todos.length,
      title: this.todoForm.value.title,
      content: this.todoForm.value.content,
      isActive: this.todoForm.value.isActive
    };
    this.todos.push(todo);
    console.log(this.todos);
  }
  onDelete(i) {
    this.todos.splice(i, 1);
  }
}
